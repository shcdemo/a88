<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A88492">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>The Londoners petition To the Right Honorable the Lords and Commons novv assembled in the high court of Parliament. The humble petition of divers inhabitants of the City of London and the liberties thereof.</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A88492 of text R212552 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.6[95]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A88492</idno>
    <idno type="STC">Wing L2914</idno>
    <idno type="STC">Thomason 669.f.6[95]</idno>
    <idno type="STC">ESTC R212552</idno>
    <idno type="EEBO-CITATION">99871158</idno>
    <idno type="PROQUEST">99871158</idno>
    <idno type="VID">160956</idno>
    <idno type="PROQUESTGOID">2248514404</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A88492)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160956)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f6[95])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>The Londoners petition To the Right Honorable the Lords and Commons novv assembled in the high court of Parliament. The humble petition of divers inhabitants of the City of London and the liberties thereof.</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for Adam Bell,</publisher>
      <pubPlace>London :</pubPlace>
      <date>[1642]</date>
     </publicationStmt>
     <notesStmt>
      <note>Publication date from Wing.</note>
      <note>Praying Parliament to "speedily tender his Majesty propositions for accomodation."</note>
      <note>Annotation on Thomason copy: "frivolous petition [illegible] ye 14th"; "Decemb: 23."</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
     <term>London (England) -- History -- 17th century -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A88492</ep:tcp>
    <ep:estc> R212552</ep:estc>
    <ep:stc> (Thomason 669.f.6[95]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>The Londoners petition. To the Right Honorable the Lords and Commons novv assembled in the high court of Parliament. The humble petition of</ep:title>
    <ep:author>anon.</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>446</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-07</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-08</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-09</date><label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change><date>2007-09</date><label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A88492-e10">
  <body xml:id="A88492-e20">
   <pb facs="tcp:160956:1" rend="simple:additions" xml:id="A88492-001-a"/>
   <div type="petition" xml:id="A88492-e30">
    <head xml:id="A88492-e40">
     <w lemma="the" pos="d" xml:id="A88492-001-a-0010">THE</w>
     <w lemma="londoner" pos="nn2" xml:id="A88492-001-a-0020">LONDONERS</w>
     <w lemma="petition" pos="n1" xml:id="A88492-001-a-0030">PETITION</w>
     <pc unit="sentence" xml:id="A88492-001-a-0040">.</pc>
     <w lemma="to" pos="acp" xml:id="A88492-001-a-0050">TO</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0060">THE</w>
     <w lemma="right" pos="n1-j" xml:id="A88492-001-a-0070">RIGHT</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A88492-001-a-0080">HONORABLE</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0090">the</w>
     <w lemma="lord" pos="n2" xml:id="A88492-001-a-0100">Lords</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-0110">and</w>
     <w lemma="commons" pos="n2" xml:id="A88492-001-a-0120">Commons</w>
     <w lemma="now" pos="av" reg="now" xml:id="A88492-001-a-0130">novv</w>
     <w lemma="assemble" pos="vvn" xml:id="A88492-001-a-0140">assembled</w>
     <w lemma="in" pos="acp" xml:id="A88492-001-a-0150">in</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0160">the</w>
     <w lemma="high" pos="j" xml:id="A88492-001-a-0170">High</w>
     <w lemma="court" pos="n1" xml:id="A88492-001-a-0180">Court</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-0190">of</w>
     <w lemma="parliament" pos="n1" xml:id="A88492-001-a-0200">Parliament</w>
     <pc unit="sentence" xml:id="A88492-001-a-0210">.</pc>
    </head>
    <head type="sub" xml:id="A88492-e50">
     <w lemma="the" pos="d" xml:id="A88492-001-a-0220">The</w>
     <w lemma="humble" pos="j" xml:id="A88492-001-a-0230">humble</w>
     <w lemma="petition" pos="n1" xml:id="A88492-001-a-0240">Petition</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-0250">of</w>
     <w lemma="divers" pos="j" xml:id="A88492-001-a-0260">divers</w>
     <w lemma="inhabitant" pos="n2" xml:id="A88492-001-a-0270">Inhabitants</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-0280">of</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0290">the</w>
     <w lemma="city" pos="n1" xml:id="A88492-001-a-0300">City</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-0310">of</w>
     <w lemma="LONDON" pos="nn1" xml:id="A88492-001-a-0320">LONDON</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-0330">and</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0340">the</w>
     <w lemma="liberty" pos="n2" xml:id="A88492-001-a-0350">Liberties</w>
     <w lemma="thereof" pos="av" xml:id="A88492-001-a-0360">thereof</w>
     <pc unit="sentence" xml:id="A88492-001-a-0370">.</pc>
    </head>
    <opener xml:id="A88492-e60">
     <w lemma="show" pos="vvz" reg="Showeth" xml:id="A88492-001-a-0380">Sheweth</w>
     <pc xml:id="A88492-001-a-0390">,</pc>
    </opener>
    <p xml:id="A88492-e70">
     <w lemma="that" pos="cs" rend="decorinit" xml:id="A88492-001-a-0400">THat</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0410">the</w>
     <w lemma="present" pos="j" xml:id="A88492-001-a-0420">present</w>
     <w lemma="sense" pos="n1" reg="sense" xml:id="A88492-001-a-0430">sence</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-0440">of</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-0450">our</w>
     <w lemma="misery" pos="n2" xml:id="A88492-001-a-0460">miseries</w>
     <pc xml:id="A88492-001-a-0470">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-0480">and</w>
     <w lemma="apprehension" pos="n1" xml:id="A88492-001-a-0490">apprehension</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-0500">of</w>
     <w lemma="inevitable" pos="j" xml:id="A88492-001-a-0510">inevitable</w>
     <w lemma="ruin" pos="n1" reg="ruin" xml:id="A88492-001-a-0520">ruine</w>
     <w lemma="both" pos="av-d" xml:id="A88492-001-a-0530">both</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-0540">of</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0550">the</w>
     <w lemma="church" pos="n1" xml:id="A88492-001-a-0560">Church</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-0570">and</w>
     <w lemma="commonwealth" pos="n1" reg="commonwealth" xml:id="A88492-001-a-0580">Common-wealth</w>
     <w lemma="make" pos="vvb" xml:id="A88492-001-a-0590">make</w>
     <w lemma="we" pos="pno" xml:id="A88492-001-a-0600">us</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-0610">to</w>
     <w lemma="become" pos="vvi" xml:id="A88492-001-a-0620">become</w>
     <w lemma="humble" pos="j" xml:id="A88492-001-a-0630">humble</w>
     <w lemma="suitor" pos="n2" xml:id="A88492-001-a-0640">suitors</w>
     <w lemma="to" pos="acp" xml:id="A88492-001-a-0650">to</w>
     <w lemma="this" pos="d" xml:id="A88492-001-a-0660">this</w>
     <w lemma="honourable" pos="j" xml:id="A88492-001-a-0670">Honourable</w>
     <w lemma="assembly" pos="n1" xml:id="A88492-001-a-0680">Assembly</w>
     <pc join="right" xml:id="A88492-001-a-0690">(</pc>
     <w lemma="the" pos="d" xml:id="A88492-001-a-0700">the</w>
     <w lemma="like" pos="js" xml:id="A88492-001-a-0710">likest</w>
     <w lemma="mean" pos="n2" reg="means" xml:id="A88492-001-a-0720">meanes</w>
     <w lemma="under" pos="acp" xml:id="A88492-001-a-0730">under</w>
     <w lemma="God" pos="nn1" xml:id="A88492-001-a-0740">God</w>
     <w lemma="for" pos="acp" xml:id="A88492-001-a-0750">for</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-0760">our</w>
     <w lemma="relief" pos="n1" reg="relief" xml:id="A88492-001-a-0770">reliefe</w>
     <pc xml:id="A88492-001-a-0780">)</pc>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-0790">to</w>
     <w lemma="consider" pos="vvi" xml:id="A88492-001-a-0800">consider</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-0810">our</w>
     <w lemma="distress" pos="j-vn" xml:id="A88492-001-a-0820">distressed</w>
     <w lemma="estate" pos="n2" xml:id="A88492-001-a-0830">estates</w>
     <pc xml:id="A88492-001-a-0840">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-0850">and</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-0860">to</w>
     <w lemma="provide" pos="vvi" xml:id="A88492-001-a-0870">provide</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-0880">a</w>
     <w lemma="speedy" pos="j" xml:id="A88492-001-a-0890">speedy</w>
     <w lemma="remedy" pos="n1" xml:id="A88492-001-a-0900">remedy</w>
     <w lemma="for" pos="acp" xml:id="A88492-001-a-0910">for</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-0920">our</w>
     <w lemma="present" pos="j" xml:id="A88492-001-a-0930">present</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-0940">and</w>
     <w lemma="future" pos="j" xml:id="A88492-001-a-0950">future</w>
     <w lemma="evil" pos="n2-j" xml:id="A88492-001-a-0960">evils</w>
     <pc xml:id="A88492-001-a-0970">,</pc>
     <w lemma="earnest" pos="av-j" xml:id="A88492-001-a-0980">earnestly</w>
     <w lemma="desire" pos="vvg" xml:id="A88492-001-a-0990">desiring</w>
     <w lemma="you" pos="pn" xml:id="A88492-001-a-1000">you</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-1010">to</w>
     <w lemma="weigh" pos="vvi" xml:id="A88492-001-a-1020">weigh</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-1030">the</w>
     <w lemma="care" pos="n1" xml:id="A88492-001-a-1040">care</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1050">and</w>
     <w lemma="judgement" pos="n1" reg="judgement" xml:id="A88492-001-a-1060">Iudgement</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-1070">of</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1080">our</w>
     <w lemma="predecessor" pos="n2" xml:id="A88492-001-a-1090">predecessors</w>
     <pc xml:id="A88492-001-a-1100">,</pc>
     <w lemma="who" pos="crq" xml:id="A88492-001-a-1110">who</w>
     <w lemma="by" pos="acp" xml:id="A88492-001-a-1120">by</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-1130">a</w>
     <w lemma="know" pos="j-vn" reg="known" xml:id="A88492-001-a-1140">knowne</w>
     <w lemma="law" pos="n1" xml:id="A88492-001-a-1150">Law</w>
     <w lemma="settle" pos="vvn" reg="settled" xml:id="A88492-001-a-1160">setled</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1170">and</w>
     <w lemma="preserve" pos="vvn" xml:id="A88492-001-a-1180">preserved</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1190">our</w>
     <w lemma="protestant" pos="jnn" xml:id="A88492-001-a-1200">Protestant</w>
     <w lemma="religion" pos="n1" xml:id="A88492-001-a-1210">Religion</w>
     <pc xml:id="A88492-001-a-1220">,</pc>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1230">our</w>
     <w lemma="liberty" pos="n2" xml:id="A88492-001-a-1240">Liberties</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1250">and</w>
     <w lemma="property" pos="n2" xml:id="A88492-001-a-1260">Properties</w>
     <pc xml:id="A88492-001-a-1270">,</pc>
     <w lemma="with" pos="acp" xml:id="A88492-001-a-1280">with</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-1290">a</w>
     <w lemma="right" pos="j" xml:id="A88492-001-a-1300">right</w>
     <w lemma="understanding" pos="n1" xml:id="A88492-001-a-1310">understanding</w>
     <w lemma="between" pos="acp" reg="between" xml:id="A88492-001-a-1320">betweene</w>
     <w lemma="king" pos="n1" xml:id="A88492-001-a-1330">King</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1340">and</w>
     <w lemma="subject" pos="n2" xml:id="A88492-001-a-1350">Subjects</w>
     <pc xml:id="A88492-001-a-1360">,</pc>
     <w lemma="which" pos="crq" xml:id="A88492-001-a-1370">which</w>
     <w lemma="produce" pos="vvn" xml:id="A88492-001-a-1380">produced</w>
     <w lemma="peace" pos="n1" xml:id="A88492-001-a-1390">peace</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1400">and</w>
     <w lemma="plenty" pos="n1" xml:id="A88492-001-a-1410">plenty</w>
     <w lemma="in" pos="acp" xml:id="A88492-001-a-1420">in</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1430">our</w>
     <w lemma="street" pos="n2" xml:id="A88492-001-a-1440">streets</w>
     <pc unit="sentence" xml:id="A88492-001-a-1450">.</pc>
    </p>
    <p xml:id="A88492-e80">
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1460">And</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-1470">to</w>
     <w lemma="reflect" pos="vvi" xml:id="A88492-001-a-1480">reflect</w>
     <w lemma="with" pos="acp" xml:id="A88492-001-a-1490">with</w>
     <w lemma="serious" pos="j" xml:id="A88492-001-a-1500">serious</w>
     <w lemma="thought" pos="n2" xml:id="A88492-001-a-1510">thoughts</w>
     <w lemma="upon" pos="acp" xml:id="A88492-001-a-1520">upon</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1530">our</w>
     <w lemma="present" pos="j" xml:id="A88492-001-a-1540">present</w>
     <w lemma="distemper" pos="n2" xml:id="A88492-001-a-1550">distempers</w>
     <pc xml:id="A88492-001-a-1560">,</pc>
     <w lemma="violate" pos="vvg" xml:id="A88492-001-a-1570">violating</w>
     <w lemma="religion" pos="n1" xml:id="A88492-001-a-1580">Religion</w>
     <w lemma="by" pos="acp" xml:id="A88492-001-a-1590">by</w>
     <w lemma="papist" pos="nn2" xml:id="A88492-001-a-1600">Papists</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1610">and</w>
     <w lemma="sectary" pos="n2" xml:id="A88492-001-a-1620">Sectaries</w>
     <pc xml:id="A88492-001-a-1630">,</pc>
     <w lemma="engage" pos="vvg" xml:id="A88492-001-a-1640">engaging</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1650">our</w>
     <w lemma="nation" pos="n1" xml:id="A88492-001-a-1660">Nation</w>
     <w lemma="into" pos="acp" xml:id="A88492-001-a-1670">into</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-1680">a</w>
     <w lemma="civil" pos="j" reg="civil" xml:id="A88492-001-a-1690">civill</w>
     <pc xml:id="A88492-001-a-1700">,</pc>
     <w lemma="bloody" pos="j" reg="bloody" xml:id="A88492-001-a-1710">bloudy</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1720">and</w>
     <w lemma="destructive" pos="j" xml:id="A88492-001-a-1730">destructive</w>
     <w lemma="war" pos="n1" xml:id="A88492-001-a-1740">war</w>
     <pc xml:id="A88492-001-a-1750">;</pc>
     <w lemma="invade" pos="vvg" xml:id="A88492-001-a-1760">invading</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1770">our</w>
     <w lemma="law" pos="n2" reg="laws" xml:id="A88492-001-a-1780">Lawes</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1790">and</w>
     <w lemma="liberty" pos="n2" xml:id="A88492-001-a-1800">Liberties</w>
     <pc xml:id="A88492-001-a-1810">,</pc>
     <w lemma="endanger" pos="vvg" xml:id="A88492-001-a-1820">endangering</w>
     <w lemma="all" pos="d" xml:id="A88492-001-a-1830">all</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1840">our</w>
     <w lemma="life" pos="n2" xml:id="A88492-001-a-1850">lives</w>
     <pc xml:id="A88492-001-a-1860">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-1870">and</w>
     <w lemma="utter" pos="av-j" xml:id="A88492-001-a-1880">utterly</w>
     <w lemma="disenable" pos="vvg" reg="disenabling" xml:id="A88492-001-a-1890">disinabling</w>
     <w lemma="we" pos="pno" xml:id="A88492-001-a-1900">us</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-1910">to</w>
     <w lemma="relieve" pos="vvi" xml:id="A88492-001-a-1920">relieve</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-1930">our</w>
     <w lemma="distress" pos="j-vn" xml:id="A88492-001-a-1940">distressed</w>
     <w lemma="brethren" pos="n2" xml:id="A88492-001-a-1950">brethren</w>
     <w lemma="in" pos="acp" xml:id="A88492-001-a-1960">in</w>
     <w lemma="Ireland" pos="nn1" xml:id="A88492-001-a-1970">Ireland</w>
     <pc unit="sentence" xml:id="A88492-001-a-1980">.</pc>
     <w lemma="we" pos="pns" xml:id="A88492-001-a-1990">We</w>
     <w lemma="beseech" pos="vvb" xml:id="A88492-001-a-2000">beseech</w>
     <w lemma="you" pos="pn" xml:id="A88492-001-a-2010">you</w>
     <w lemma="likewise" pos="av" xml:id="A88492-001-a-2020">likewise</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-2030">to</w>
     <w lemma="consider" pos="vvi" xml:id="A88492-001-a-2040">consider</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-2050">the</w>
     <w lemma="effect" pos="n2" xml:id="A88492-001-a-2060">effects</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-2070">of</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-2080">a</w>
     <w lemma="continue" pos="j-vn" xml:id="A88492-001-a-2090">continued</w>
     <w lemma="war" pos="n1" xml:id="A88492-001-a-2100">war</w>
     <pc xml:id="A88492-001-a-2110">,</pc>
     <w lemma="as" pos="acp" xml:id="A88492-001-a-2120">as</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-2130">the</w>
     <w lemma="destruction" pos="n1" xml:id="A88492-001-a-2140">destruction</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-2150">of</w>
     <w lemma="christian" pos="nn2" xml:id="A88492-001-a-2160">Christians</w>
     <pc xml:id="A88492-001-a-2170">,</pc>
     <w lemma="the" pos="d" xml:id="A88492-001-a-2180">the</w>
     <w lemma="unnatural" pos="j" reg="unnatural" xml:id="A88492-001-a-2190">unnaturall</w>
     <w lemma="effusion" pos="n1" xml:id="A88492-001-a-2200">effusion</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-2210">of</w>
     <w lemma="blood" pos="n1" reg="blood" xml:id="A88492-001-a-2220">bloud</w>
     <pc xml:id="A88492-001-a-2230">,</pc>
     <w lemma="father" pos="n2" xml:id="A88492-001-a-2240">Fathers</w>
     <w lemma="against" pos="acp" xml:id="A88492-001-a-2250">against</w>
     <w lemma="son" pos="n2" reg="sons" xml:id="A88492-001-a-2260">Sonnes</w>
     <pc xml:id="A88492-001-a-2270">,</pc>
     <w lemma="brother" pos="n2" xml:id="A88492-001-a-2280">brothers</w>
     <w lemma="by" pos="acp" xml:id="A88492-001-a-2290">by</w>
     <w lemma="brother" pos="n2" xml:id="A88492-001-a-2300">brothers</w>
     <pc xml:id="A88492-001-a-2310">,</pc>
     <w lemma="friend" pos="n2" xml:id="A88492-001-a-2320">friends</w>
     <w lemma="by" pos="acp" xml:id="A88492-001-a-2330">by</w>
     <w lemma="friend" pos="n2" xml:id="A88492-001-a-2340">friends</w>
     <w lemma="slay" pos="vvn" reg="slain" xml:id="A88492-001-a-2350">slaine</w>
     <pc xml:id="A88492-001-a-2360">;</pc>
     <w lemma="then" pos="av" xml:id="A88492-001-a-2370">then</w>
     <pc xml:id="A88492-001-a-2380">,</pc>
     <w lemma="famine" pos="n1" xml:id="A88492-001-a-2390">famine</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-2400">and</w>
     <w lemma="sickness" pos="n1" reg="sickness" xml:id="A88492-001-a-2410">sicknesse</w>
     <pc xml:id="A88492-001-a-2420">,</pc>
     <w lemma="the" pos="d" xml:id="A88492-001-a-2430">the</w>
     <w lemma="follower" pos="n2" xml:id="A88492-001-a-2440">followers</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-2450">of</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-2460">a</w>
     <w lemma="civil" pos="j" reg="civil" xml:id="A88492-001-a-2470">Civill</w>
     <w lemma="war" pos="n1" xml:id="A88492-001-a-2480">war</w>
     <pc xml:id="A88492-001-a-2490">,</pc>
     <w lemma="make" pos="vvg" xml:id="A88492-001-a-2500">making</w>
     <w lemma="way" pos="n1" xml:id="A88492-001-a-2510">way</w>
     <w lemma="for" pos="acp" xml:id="A88492-001-a-2520">for</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-2530">a</w>
     <w lemma="general" pos="j" reg="general" xml:id="A88492-001-a-2540">generall</w>
     <w lemma="confusion" pos="n1" xml:id="A88492-001-a-2550">confusion</w>
     <pc xml:id="A88492-001-a-2560">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-2570">and</w>
     <w lemma="invasion" pos="n1" xml:id="A88492-001-a-2580">invasion</w>
     <w lemma="by" pos="acp" xml:id="A88492-001-a-2590">by</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-2600">a</w>
     <w lemma="foreign" pos="j" reg="foreign" xml:id="A88492-001-a-2610">forraigne</w>
     <w lemma="nation" pos="n1" xml:id="A88492-001-a-2620">Nation</w>
     <pc xml:id="A88492-001-a-2630">,</pc>
     <w lemma="while" pos="cs" xml:id="A88492-001-a-2640">while</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-2650">our</w>
     <w lemma="treasure" pos="n1" xml:id="A88492-001-a-2660">Treasure</w>
     <w lemma="be" pos="vvz" xml:id="A88492-001-a-2670">is</w>
     <w lemma="exhaust" pos="vvn" xml:id="A88492-001-a-2680">exhausted</w>
     <pc xml:id="A88492-001-a-2690">,</pc>
     <w lemma="our" pos="po" xml:id="A88492-001-a-2700">our</w>
     <w lemma="trade" pos="n1" xml:id="A88492-001-a-2710">Trade</w>
     <w lemma="lose" pos="vvn" xml:id="A88492-001-a-2720">lost</w>
     <pc xml:id="A88492-001-a-2730">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-2740">and</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-2750">the</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A88492-001-a-2760">Kingdome</w>
     <w lemma="dispeople" pos="vvn" xml:id="A88492-001-a-2770">dispeopled</w>
     <pc unit="sentence" xml:id="A88492-001-a-2780">.</pc>
     <w lemma="these" pos="d" xml:id="A88492-001-a-2790">These</w>
     <w lemma="thing" pos="n2" xml:id="A88492-001-a-2800">things</w>
     <w lemma="weigh" pos="vvn" xml:id="A88492-001-a-2810">weighed</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-2820">and</w>
     <w lemma="enlarge" pos="vvn" reg="enlarged" xml:id="A88492-001-a-2830">inlarged</w>
     <w lemma="by" pos="acp" xml:id="A88492-001-a-2840">by</w>
     <w lemma="your" pos="po" xml:id="A88492-001-a-2850">your</w>
     <w lemma="wisdom" pos="n2" reg="wisdoms" xml:id="A88492-001-a-2860">wisdomes</w>
     <pc xml:id="A88492-001-a-2870">,</pc>
     <w lemma="we" pos="pns" xml:id="A88492-001-a-2880">we</w>
     <w lemma="doubt" pos="vvb" xml:id="A88492-001-a-2890">doubt</w>
     <w lemma="not" pos="xx" xml:id="A88492-001-a-2900">not</w>
     <w lemma="will" pos="vmb" xml:id="A88492-001-a-2910">will</w>
     <w lemma="be" pos="vvi" xml:id="A88492-001-a-2920">be</w>
     <w lemma="as" pos="acp" xml:id="A88492-001-a-2930">as</w>
     <w lemma="strong" pos="j" xml:id="A88492-001-a-2940">strong</w>
     <w lemma="motive" pos="n2" xml:id="A88492-001-a-2950">motives</w>
     <w lemma="in" pos="acp" xml:id="A88492-001-a-2960">in</w>
     <w lemma="you" pos="pn" xml:id="A88492-001-a-2970">you</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-2980">to</w>
     <w lemma="labour" pos="vvi" xml:id="A88492-001-a-2990">labour</w>
     <pc xml:id="A88492-001-a-3000">,</pc>
     <w lemma="as" pos="acp" xml:id="A88492-001-a-3010">as</w>
     <w lemma="in" pos="acp" xml:id="A88492-001-a-3020">in</w>
     <w lemma="we" pos="pno" xml:id="A88492-001-a-3030">us</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-3040">to</w>
     <w lemma="desire" pos="vvi" xml:id="A88492-001-a-3050">desire</w>
     <pc xml:id="A88492-001-a-3060">,</pc>
     <w lemma="a" pos="d" xml:id="A88492-001-a-3070">a</w>
     <w lemma="speedy" pos="j" xml:id="A88492-001-a-3080">speedy</w>
     <w lemma="peace" pos="n1" xml:id="A88492-001-a-3090">Peace</w>
     <pc xml:id="A88492-001-a-3100">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-3110">and</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-3120">a</w>
     <w lemma="happy" pos="j" xml:id="A88492-001-a-3130">happy</w>
     <w lemma="accommodation" pos="n1" xml:id="A88492-001-a-3140">Accommodation</w>
     <pc unit="sentence" xml:id="A88492-001-a-3150">.</pc>
    </p>
    <p xml:id="A88492-e90">
     <w lemma="wherefore" pos="crq" reg="Wherefore" xml:id="A88492-001-a-3160">Wherfore</w>
     <w lemma="we" pos="pns" xml:id="A88492-001-a-3170">we</w>
     <w lemma="humble" pos="av-j" xml:id="A88492-001-a-3180">humbly</w>
     <w lemma="pray" pos="vvb" xml:id="A88492-001-a-3190">pray</w>
     <pc xml:id="A88492-001-a-3200">,</pc>
     <w lemma="that" pos="cs" xml:id="A88492-001-a-3210">that</w>
     <pc join="right" xml:id="A88492-001-a-3220">(</pc>
     <w lemma="not" pos="xx" xml:id="A88492-001-a-3230">not</w>
     <w lemma="lend" pos="vvg" xml:id="A88492-001-a-3240">lending</w>
     <w lemma="a" pos="d" xml:id="A88492-001-a-3250">an</w>
     <w lemma="ear" pos="n1" reg="ear" xml:id="A88492-001-a-3260">eare</w>
     <w lemma="to" pos="acp" xml:id="A88492-001-a-3270">to</w>
     <w lemma="any" pos="d" xml:id="A88492-001-a-3280">any</w>
     <w lemma="fomenter" pos="n2" xml:id="A88492-001-a-3290">Fomenters</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-3300">of</w>
     <w lemma="these" pos="d" xml:id="A88492-001-a-3310">these</w>
     <w lemma="present" pos="j" xml:id="A88492-001-a-3320">present</w>
     <w lemma="war" pos="n2" xml:id="A88492-001-a-3330">Wars</w>
     <pc xml:id="A88492-001-a-3340">,</pc>
     <w lemma="under" pos="acp" xml:id="A88492-001-a-3350">under</w>
     <w lemma="what" pos="crq" xml:id="A88492-001-a-3360">what</w>
     <w lemma="pretence" pos="n1" xml:id="A88492-001-a-3370">pretence</w>
     <w lemma="soever" pos="av" xml:id="A88492-001-a-3380">soever</w>
     <pc xml:id="A88492-001-a-3390">,</pc>
     <w lemma="nor" pos="ccx" xml:id="A88492-001-a-3400">nor</w>
     <w lemma="remember" pos="vvg" reg="remembering" xml:id="A88492-001-a-3410">remembring</w>
     <w lemma="aught" pos="pi" reg="aught" xml:id="A88492-001-a-3420">ought</w>
     <w lemma="that" pos="cs" xml:id="A88492-001-a-3430">that</w>
     <w lemma="may" pos="vmb" xml:id="A88492-001-a-3440">may</w>
     <w lemma="increase" pos="vvi" reg="increase" xml:id="A88492-001-a-3450">encrease</w>
     <w lemma="jealousy" pos="n2" reg="jealousies" xml:id="A88492-001-a-3460">Iealousies</w>
     <pc xml:id="A88492-001-a-3470">,</pc>
     <w lemma="or" pos="cc" xml:id="A88492-001-a-3480">or</w>
     <w lemma="continue" pos="vvi" xml:id="A88492-001-a-3490">continue</w>
     <w lemma="division" pos="n2" xml:id="A88492-001-a-3500">divisions</w>
     <w lemma="between" pos="acp" reg="between" xml:id="A88492-001-a-3510">betweene</w>
     <w lemma="his" pos="po" xml:id="A88492-001-a-3520">his</w>
     <w lemma="majesty" pos="n1" xml:id="A88492-001-a-3530">Majesty</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-3540">and</w>
     <w lemma="his" pos="po" xml:id="A88492-001-a-3550">his</w>
     <w lemma="house" pos="n1" xml:id="A88492-001-a-3560">House</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-3570">of</w>
     <w lemma="parliament" pos="n1" xml:id="A88492-001-a-3580">Parliament</w>
     <pc xml:id="A88492-001-a-3590">)</pc>
     <w lemma="you" pos="pn" xml:id="A88492-001-a-3600">you</w>
     <w lemma="will" pos="vmb" xml:id="A88492-001-a-3610">will</w>
     <w lemma="speedy" pos="av-j" xml:id="A88492-001-a-3620">speedily</w>
     <w lemma="tender" pos="vvi" xml:id="A88492-001-a-3630">tender</w>
     <w lemma="his" pos="po" xml:id="A88492-001-a-3640">his</w>
     <w lemma="majesty" pos="n1" xml:id="A88492-001-a-3650">Majesty</w>
     <pc join="right" xml:id="A88492-001-a-3660">(</pc>
     <w lemma="according" pos="j" xml:id="A88492-001-a-3670">according</w>
     <w lemma="to" pos="acp" xml:id="A88492-001-a-3680">to</w>
     <w lemma="his" pos="po" xml:id="A88492-001-a-3690">his</w>
     <w lemma="royal" pos="j" reg="royal" xml:id="A88492-001-a-3700">Royall</w>
     <w lemma="intimation" pos="n2" xml:id="A88492-001-a-3710">Intimations</w>
     <pc xml:id="A88492-001-a-3720">)</pc>
     <w lemma="such" pos="d" xml:id="A88492-001-a-3730">such</w>
     <w lemma="proposition" pos="n2" xml:id="A88492-001-a-3740">propositions</w>
     <w lemma="for" pos="acp" xml:id="A88492-001-a-3750">for</w>
     <w lemma="accommodation" pos="n1" xml:id="A88492-001-a-3760">Accommodation</w>
     <pc xml:id="A88492-001-a-3770">,</pc>
     <w lemma="as" pos="acp" xml:id="A88492-001-a-3780">as</w>
     <w lemma="he" pos="pns" reg="he" xml:id="A88492-001-a-3790">Hee</w>
     <w lemma="may" pos="vmb" xml:id="A88492-001-a-3800">may</w>
     <w lemma="with" pos="acp" xml:id="A88492-001-a-3810">with</w>
     <w lemma="honour" pos="n1" xml:id="A88492-001-a-3820">Honour</w>
     <pc xml:id="A88492-001-a-3830">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-3840">and</w>
     <w lemma="safety" pos="n1" xml:id="A88492-001-a-3850">safety</w>
     <w lemma="to" pos="acp" xml:id="A88492-001-a-3860">to</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-3870">the</w>
     <w lemma="whole" pos="j" xml:id="A88492-001-a-3880">whole</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A88492-001-a-3890">Kingdome</w>
     <pc xml:id="A88492-001-a-3900">,</pc>
     <w lemma="accept" pos="vvb" xml:id="A88492-001-a-3910">accept</w>
     <pc unit="sentence" xml:id="A88492-001-a-3920">.</pc>
     <w lemma="for" pos="acp" xml:id="A88492-001-a-3930">For</w>
     <w lemma="effect" pos="vvg" xml:id="A88492-001-a-3940">effecting</w>
     <w lemma="whereof" pos="crq" xml:id="A88492-001-a-3950">whereof</w>
     <w lemma="we" pos="pns" xml:id="A88492-001-a-3960">we</w>
     <w lemma="shall" pos="vmb" xml:id="A88492-001-a-3970">shall</w>
     <w lemma="be" pos="vvi" xml:id="A88492-001-a-3980">be</w>
     <w lemma="ready" pos="j" xml:id="A88492-001-a-3990">ready</w>
     <w lemma="to" pos="prt" xml:id="A88492-001-a-4000">to</w>
     <w lemma="assist" pos="vvi" xml:id="A88492-001-a-4010">assist</w>
     <w lemma="you" pos="pn" xml:id="A88492-001-a-4020">you</w>
     <w lemma="with" pos="acp" xml:id="A88492-001-a-4030">with</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-4040">the</w>
     <w lemma="best" pos="js" xml:id="A88492-001-a-4050">best</w>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-4060">and</w>
     <w lemma="utmost" pos="j" xml:id="A88492-001-a-4070">utmost</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-4080">of</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-4090">our</w>
     <w lemma="ability" pos="n2" xml:id="A88492-001-a-4100">abilities</w>
     <pc xml:id="A88492-001-a-4110">;</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-4120">and</w>
     <w lemma="while" pos="cs" reg="whilst" xml:id="A88492-001-a-4130">whilest</w>
     <w lemma="you" pos="pn" xml:id="A88492-001-a-4140">you</w>
     <w lemma="endeavour" pos="vvb" reg="endeavour" xml:id="A88492-001-a-4150">endeavor</w>
     <w lemma="peace" pos="n1" xml:id="A88492-001-a-4160">peace</w>
     <pc xml:id="A88492-001-a-4170">,</pc>
     <w lemma="we" pos="pns" xml:id="A88492-001-a-4180">we</w>
     <w lemma="shall" pos="vmb" xml:id="A88492-001-a-4190">shall</w>
     <w lemma="send" pos="vvi" xml:id="A88492-001-a-4200">send</w>
     <w lemma="up" pos="acp" xml:id="A88492-001-a-4210">up</w>
     <w lemma="our" pos="po" xml:id="A88492-001-a-4220">our</w>
     <w lemma="prayer" pos="n2" xml:id="A88492-001-a-4230">prayers</w>
     <w lemma="to" pos="acp" xml:id="A88492-001-a-4240">to</w>
     <w lemma="heaven" pos="n1" xml:id="A88492-001-a-4250">Heaven</w>
     <pc xml:id="A88492-001-a-4260">,</pc>
     <w lemma="for" pos="acp" xml:id="A88492-001-a-4270">for</w>
     <w lemma="the" pos="d" xml:id="A88492-001-a-4280">the</w>
     <w lemma="blessing" pos="n1" xml:id="A88492-001-a-4290">blessing</w>
     <w lemma="of" pos="acp" xml:id="A88492-001-a-4300">of</w>
     <w lemma="peace" pos="n1" xml:id="A88492-001-a-4310">peace</w>
     <w lemma="upon" pos="acp" xml:id="A88492-001-a-4320">upon</w>
     <w lemma="you" pos="pn" xml:id="A88492-001-a-4330">you</w>
     <pc xml:id="A88492-001-a-4340">,</pc>
     <w lemma="and" pos="cc" xml:id="A88492-001-a-4350">and</w>
     <w lemma="all" pos="d" xml:id="A88492-001-a-4360">all</w>
     <w lemma="that" pos="cs" xml:id="A88492-001-a-4370">that</w>
     <w lemma="desire" pos="vvb" xml:id="A88492-001-a-4380">desire</w>
     <w lemma="it" pos="pn" xml:id="A88492-001-a-4390">it</w>
     <pc unit="sentence" xml:id="A88492-001-a-4400">.</pc>
    </p>
   </div>
  </body>
  <back xml:id="A88492-e100">
   <div type="colophon" xml:id="A88492-e110">
    <p xml:id="A88492-e120">
     <hi xml:id="A88492-e130">
      <w lemma="london" pos="nn1" xml:id="A88492-001-a-4410">London</w>
      <pc xml:id="A88492-001-a-4420">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A88492-001-a-4430">Printed</w>
     <w lemma="for" pos="acp" xml:id="A88492-001-a-4440">for</w>
     <hi xml:id="A88492-e140">
      <w lemma="Adam" pos="nn1" xml:id="A88492-001-a-4450">Adam</w>
      <w lemma="Bell." pos="ab" xml:id="A88492-001-a-4460">Bell.</w>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
